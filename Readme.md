# Statemachine

This is just a simple project to demonstrate various implementation techniques for a state machine. There are solution for the programming language C only at the moment

# Prerequisites

- CMake (>= 2.8.12)
- GCC (>= 4.8)

LICENSE

Please see the file called [LICENSE](LICENSE). 
