/*
 * Copyright (C) 2016 Christian Ege
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distribted on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/**
 * The inernal states
 */
typedef enum {
    STATE_OBEN         = 0,
    STATE_UNTEN        = 1,
    STATE_FAHRE_HOCH   = 2,
    STATE_FAHRE_RUNTER = 3,
    STATE_ERROR        = 4,
    STATE_MAX
} StatemachineState_t;

typedef enum {
    EV_TASTE               = 0,
    EV_ENDSCHALTER_OBEN    = 1,
    EV_ENDSCHALTER_UNTEN   = 2,
    EV_MAX
} Event_t;


int error(Event_t e)
{
    printf("Error detected! Event: %d\n",e);
    return EXIT_FAILURE;
}

int state_oben(Event_t e)
{
    printf("Aktueller Zustand: OBEN Event: %d\n",e);
    return EXIT_SUCCESS;
}

int state_unten(Event_t e)
{
    printf("Aktueller Zustand: UNTEN Event: %d\n",e);
    return EXIT_SUCCESS;
}

int state_fahre_hoch(Event_t e)
{
    printf("Aktueller Zustand: FAHRE_HOCH Event: %d\n",e);
    return EXIT_SUCCESS;
}

int state_fahre_runter(Event_t e)
{
    printf("Aktueller Zustand: FAHRE_RUNTER Event: %d \n",e);
    return EXIT_SUCCESS;
}

typedef int (*stateHandler_t)(Event_t e);

typedef struct {
    StatemachineState_t nextState;
    stateHandler_t hdl;
}StateTuple_t;

typedef struct MyState{
    StatemachineState_t state; /*< The state   */
} MyState_t;



static StateTuple_t TransitionTable[EV_MAX][STATE_MAX] = {
    /*                             STATE_OBEN                                STATE_UNTEN                           STATE_FAHRE_HOCH                          STATE_FAHRE_RUNTER     */ 
    /* EV_TASTE              */ { {STATE_FAHRE_RUNTER,state_fahre_runter},  {STATE_FAHRE_HOCH,state_fahre_hoch},  {STATE_FAHRE_RUNTER,state_fahre_runter},  {STATE_FAHRE_HOCH,state_fahre_hoch} },
    /* EV_ENDSCHALTER_OBEN   */ { {STATE_OBEN,state_oben},                  {STATE_ERROR,error},                  {STATE_OBEN,state_oben},                  {STATE_ERROR,error}                 },
    /* EV_ENDSCHALTER_UNTEN  */ { {STATE_ERROR,error},                      {STATE_UNTEN,state_unten},            {STATE_ERROR,error},                      {STATE_UNTEN,state_unten}           }
};


MyState_t* myStatemachine_create(void)
{
    MyState_t* ret = (MyState_t*)malloc(sizeof(MyState_t));
    if(NULL != ret)
    {
        memset(ret,0,sizeof(MyState_t));
        ret->state = STATE_FAHRE_HOCH;
    }
    return ret;
}

void myStatemachine_free(MyState_t* statemachine)
{
    if(NULL != statemachine)
    {
        free(statemachine);
    }
}

int myStatemachine_handle(MyState_t* statemachine, Event_t event)
{
    int ret = 1;
    if(   ((STATE_OBEN <= statemachine->state) && (STATE_MAX > statemachine->state)))
    {
        StateTuple_t state = TransitionTable[event][statemachine->state];
        statemachine->state = state.nextState;
        ret = state.hdl(event);
    }
    return ret;
}

int main(int argc, char** argv)
{
    Event_t test[] = { EV_ENDSCHALTER_OBEN,
        EV_TASTE,
        EV_ENDSCHALTER_UNTEN,
    };
    int i = 0;

    MyState_t* machine = myStatemachine_create();

    for(i = 0; i < sizeof(test)/sizeof(Event_t); i++)
    {
        myStatemachine_handle(machine,test[i]);
    }

    myStatemachine_free(machine);
    return 0;
}
